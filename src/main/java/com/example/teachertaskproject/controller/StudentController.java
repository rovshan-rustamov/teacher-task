package com.example.teachertaskproject.controller;

import com.example.teachertaskproject.dto.CreateStudentDto;
import com.example.teachertaskproject.dto.StudentDto;
import com.example.teachertaskproject.dto.UpdateStudentDto;
import com.example.teachertaskproject.model.Student;
import com.example.teachertaskproject.services.StudentService;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/student")
public class StudentController {

    private final StudentService studentService;

    public StudentController(StudentService studentService) {
        this.studentService = studentService;
    }

    @PostMapping
    public String create(@RequestBody CreateStudentDto student){
        studentService.create(student);
        return "Successfully posted";
    }
    @PutMapping
    public void update(@RequestBody UpdateStudentDto student){
        studentService.update(student);
    }

    @GetMapping("/{id}")
    public StudentDto get(@PathVariable Integer id){
        return studentService.get(id);
    }

    @GetMapping
    public List<StudentDto> getAll(){
        return studentService.getAll();
    }
    @DeleteMapping("/{id}")
    public void delete(@PathVariable Integer id){
        studentService.delete(id);
    }
}
