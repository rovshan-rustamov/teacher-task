package com.example.teachertaskproject.controller;

import com.example.teachertaskproject.model.Student;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/teachertask")
public class GreetingController {
    @GetMapping
    public String sayHello() {
        return "Hello";
    }
    @PostMapping
    public String create(@RequestBody Student student){
        System.out.println(student.getSurname());
        System.out.println(student.getId());
        return "Hello, " + student.getName();
    }
    @PutMapping
    public void updateStudent(@RequestBody Student student){
        System.out.println("Student updated");
    }
    @DeleteMapping("/{id}")
    public String deleteStudent(@PathVariable Integer id){
        return "Student with " + id + " ID" + " is deleted";
    }

}
