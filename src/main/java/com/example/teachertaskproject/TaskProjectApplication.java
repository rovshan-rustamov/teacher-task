package com.example.teachertaskproject;

import com.example.teachertaskproject.configuration.AppConfig;
import com.example.teachertaskproject.model.Student;
import com.example.teachertaskproject.services.StudentService;
import com.example.teachertaskproject.services.TeacherService;
import com.example.teachertaskproject.services.TeacherService2Impl;
import com.example.teachertaskproject.services.TeacherServiceImpl;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;

@SpringBootApplication
@AllArgsConstructor

public class TaskProjectApplication implements CommandLineRunner {

//	@Value("${ms.name}")
//    private String name;
	private final AppConfig appConfig;
	private final ApplicationContext applicationContext;
	public static void main(String[] args) {
		SpringApplication.run(TaskProjectApplication.class, args);
	}

	@Override
	public void run(String... args) throws Exception {


	}
}
