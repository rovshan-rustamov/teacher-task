package com.example.teachertaskproject.services;

import com.example.teachertaskproject.dto.CreateStudentDto;
import com.example.teachertaskproject.dto.StudentDto;
import com.example.teachertaskproject.dto.UpdateStudentDto;
import com.example.teachertaskproject.model.Student;
import com.example.teachertaskproject.repository.StudentRepository;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class StudentService {
    

    private final StudentRepository studentRepository;
    private final ModelMapper modelMapper;

    public StudentService(StudentRepository studentRepository, ModelMapper modelMapper) {
        this.modelMapper = modelMapper;
        this.studentRepository = studentRepository;
    }

    public void create(CreateStudentDto dto) {
//        var student = new Student();
//        student.setName(dto.getName());
//        student.setSurname(dto.getSurname());
        Student student = modelMapper.map(dto, Student.class);
        studentRepository.save(student);
    }

    public void update(UpdateStudentDto student) {
        Optional<Student> entity = studentRepository.findById(student.getId());
        entity.ifPresent(student1 -> {
            student1.setName(student.getName());
            student1.setSurname(student.getSurname());
            studentRepository.save(student1);
        });

    }
    public List<StudentDto> getAll(){   // TODO exercise (Return as StudentDto)
        List<Student> studentList = studentRepository.findAll();
        List<StudentDto> studentDtoList = studentList
                .stream()
                .map(student -> modelMapper.map(student, StudentDto.class))
                .collect(Collectors.toList());
        return studentDtoList;
    }

    public StudentDto get(Integer id) {
        Student student = studentRepository.findById(id).get();
//        StudentDto studentDto = new StudentDto();
//        student.setId(student.getId());
//        studentDto.setName(student.getName());
//        studentDto.setSurname(student.getSurname());
        StudentDto studentDto = modelMapper.map(student, StudentDto.class);
        return studentDto;

    }

    public void delete(Integer id) {
        studentRepository.deleteById(id);
    }
}
