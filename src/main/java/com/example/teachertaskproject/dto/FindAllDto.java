package com.example.teachertaskproject.dto;

import lombok.Data;

@Data
public class FindAllDto {
    String name;
    String surname;
    Integer id;
}
