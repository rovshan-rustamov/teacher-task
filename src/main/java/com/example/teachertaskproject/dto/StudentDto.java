package com.example.teachertaskproject.dto;

import lombok.Data;

@Data

public class StudentDto {
    Integer id;
    String name;
    String surname;

}
