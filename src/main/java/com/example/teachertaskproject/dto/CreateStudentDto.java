package com.example.teachertaskproject.dto;

import lombok.Data;

@Data

public class CreateStudentDto {

    String name;
    String surname;
    String password;
}
