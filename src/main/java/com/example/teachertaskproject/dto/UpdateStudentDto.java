package com.example.teachertaskproject.dto;

import lombok.Data;

@Data

public class UpdateStudentDto {
    Integer id;
    String name;
    String surname;

}
