package com.example.teachertaskproject.model;

public final class SingletonClass {
    private static SingletonClass singletonClass;

    private SingletonClass() {

    }

    public static SingletonClass getSingletonClass() {
        if (singletonClass == null) {
            synchronized (SingletonClass.class) {
                if (singletonClass == null) {
                    singletonClass = new SingletonClass();
                }
            }
        }
        return singletonClass;
    }
}
