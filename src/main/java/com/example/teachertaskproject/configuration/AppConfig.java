package com.example.teachertaskproject.configuration;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

import java.util.List;

@Configuration
@ConfigurationProperties("ms")
@Data
public class AppConfig {
    private String name;
    private String teacher;
    private List<String> students;
}
