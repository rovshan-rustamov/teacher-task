package com.example.teachertaskproject.configuration;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Configurable;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class Config {
    @Bean
    // @Scope("singleton")
    // @Scope("prototype")
    public ModelMapper getModelMapper(){
        return new ModelMapper();
    }
}
