package com.example.teachertaskproject.repository;

import com.example.teachertaskproject.model.Student;
import org.springframework.data.jpa.repository.JpaRepository;

public interface StudentRepository extends JpaRepository<Student, Integer> {
}
